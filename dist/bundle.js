/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./main/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./main/script.js":
/*!************************!*\
  !*** ./main/script.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// fonction Jquery d'écoute du bouton valider\r\nvar addButton = $(\"#addButton\");\r\n\r\naddButton.on(\"click\", function () {\r\n    var taskTextBox = $('#taskTextBox'); //on met le textbox  dans une variable\r\n    var newTaskInput = taskTextBox.val(); // on récupère le contenu du textbox\r\n    if (newTaskInput.length < 5) { // un test sur le contenu récupéré\r\n        alert(\" Une tâche doit au moins comporter 6 caractères. Merci de réessayer.\")\r\n    }\r\n    else {\r\n        taskTextBox.val(\"\"); // on vide le contenu de la textbox\r\n        // on crée un objet jQuery contenant une balise li contenant 2 nodes : un texte, et une croix\r\n        var newTaskContent = $(\r\n            '<li class=\"task list-group-item\" >' +\r\n            '<span class=\"taskTextNode\">' + newTaskInput + '</span> ' +\r\n            '<span class=\"closingCrossNode\" onclick=\"$(this).parent().remove()\"> \\u00D7 </span>' +\r\n            '</li>'\r\n        );\r\n        $('#ListToBeDone').append(newTaskContent); // on insère notre objet Jquery li dans le jsquery ul\r\n    }\r\n});\r\n\r\n// permet d'écouter également l'appuie de la touche enter\r\n$('#taskTextBox').keydown(function (e) {\r\n    if (e.which === 13) {\r\n        addButton.click();\r\n    }\r\n});\r\n\r\n// évite que la page recharge quand on presse enter\r\n$(\"#userForm\").submit(function() {\r\n    return false;\r\n});\r\n\r\n// écouter le clic sur les tasks de liste TO BE DONE\r\n$('#ListToBeDone').on('click', '.task', function (e) {\r\n    var myTask = $(this).detach(); // on extrait la tâche de la liste\r\n    $('#ListDone').append(myTask); // on la place dans l'autre liste\r\n});\r\n\r\n// écouter le clic sur les tasks de liste DONE\r\n$('#ListDone').on('click', '.task', function (e) {\r\n    var myTask = $(this).detach();\r\n    $('#ListToBeDone').append(myTask);\r\n});\r\n\r\n\n\n//# sourceURL=webpack:///./main/script.js?");

/***/ })

/******/ });