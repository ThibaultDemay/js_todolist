// fonction Jquery d'écoute du bouton valider
var addButton = $("#addButton");

addButton.on("click", function () {
    var taskTextBox = $('#taskTextBox'); //on met le textbox  dans une variable
    var newTaskInput = taskTextBox.val(); // on récupère le contenu du textbox
    if (newTaskInput.length < 5) { // un test sur le contenu récupéré
        alert(" Une tâche doit au moins comporter 6 caractères. Merci de réessayer.")
    }
    else {
        taskTextBox.val(""); // on vide le contenu de la textbox
        // on crée un objet jQuery contenant une balise li contenant 2 nodes : un texte, et une croix
        var newTaskContent = $(
            '<li class="task list-group-item" >' +
            '<span class="taskTextNode">' + newTaskInput + '</span> ' +
            '<span class="closingCrossNode" onclick="$(this).parent().remove()"> \u00D7 </span>' +
            '</li>'
        );
        $('#ListToBeDone').append(newTaskContent); // on insère notre objet Jquery li dans le jsquery ul
    }
});

// permet d'écouter également l'appuie de la touche enter
$('#taskTextBox').keydown(function (e) {
    if (e.which === 13) {
        addButton.click();
    }
});

// évite que la page recharge quand on presse enter
$("#userForm").submit(function() {
    return false;
});

// écouter le clic sur les tasks de liste TO BE DONE
$('#ListToBeDone').on('click', '.task', function (e) {
    var myTask = $(this).detach(); // on extrait la tâche de la liste
    $('#ListDone').append(myTask); // on la place dans l'autre liste
});

// écouter le clic sur les tasks de liste DONE
$('#ListDone').on('click', '.task', function (e) {
    var myTask = $(this).detach();
    $('#ListToBeDone').append(myTask);
});

